FROM eclipse-temurin:17.0.1_12-jre-alpine
VOLUME /redis-service/logs
ARG JAR_FILE=build/libs/devops-demo-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} devops-demo-with-redis.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "devops-demo-with-redis.jar"]