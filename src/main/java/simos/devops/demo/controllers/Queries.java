package simos.devops.demo.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import simos.devops.demo.services.QueryHit;


@RestController @RequestMapping("history")
@RequiredArgsConstructor(onConstructor=@__({ @Autowired }))
public class Queries {
	
	private final QueryHit queryHit;
	
	@GetMapping
	public ResponseEntity<Integer> hit(){
		return new ResponseEntity<>(queryHit.getHit(), HttpStatus.OK);
	}
	
}

