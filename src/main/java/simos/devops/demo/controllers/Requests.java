package simos.devops.demo.controllers;

import simos.devops.demo.models.Hit;
import simos.devops.demo.services.RequestHit;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController @RequestMapping("hit")
@RequiredArgsConstructor(onConstructor=@__({ @Autowired }))
public class Requests {
	
	private final RequestHit requestHit;
	
	@GetMapping
	public ResponseEntity<Hit> hit(){
		return new ResponseEntity<>(requestHit.addHit(new Hit(LocalDateTime.now())), HttpStatus.OK);
	}
	
}
