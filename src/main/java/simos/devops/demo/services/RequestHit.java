package simos.devops.demo.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simos.devops.demo.models.Hit;
import simos.devops.demo.repository.RedisRepositoryImpl;

@Service
@AllArgsConstructor(onConstructor=@__({ @Autowired }))
public class RequestHit {
	
	private RedisRepositoryImpl redis;
	
	public Hit addHit(final Hit hit){
		redis.add(hit);
		return hit;
	}
	
}
