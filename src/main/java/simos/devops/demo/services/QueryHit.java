package simos.devops.demo.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simos.devops.demo.repository.RedisRepositoryImpl;

@Service
@AllArgsConstructor(onConstructor=@__({ @Autowired}))
public class QueryHit {
	
	private RedisRepositoryImpl redis;
	
	public Integer getHit(){
		return redis.get();
	}
}
