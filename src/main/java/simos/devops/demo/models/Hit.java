package simos.devops.demo.models;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor(onConstructor=@__({ @Autowired }))
@NoArgsConstructor(onConstructor=@__({ @Autowired }))
public class Hit {
	
	private LocalDateTime hitTime;

}
