package simos.devops.demo.repository;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;
import simos.devops.demo.models.Hit;

import java.time.LocalDateTime;

@Repository
@AllArgsConstructor(onConstructor=@__({ @Autowired }))
public class RedisRepositoryImpl {
	
	private static final String KEY = "HIT";
	private static final String HIT_COUNTER = "COUNTER";

	private HashOperations<String , Integer, LocalDateTime> hashOps;
	private ValueOperations<String, Integer> counterOps;

	public void add(final Hit hit){
		hashOps.put(KEY, hit.hashCode(), hit.getHitTime());
		incr();
	}
	
	private void incr(){ counterOps.increment(HIT_COUNTER); }
	
	public Integer get(){ return counterOps.get(HIT_COUNTER); }
	
}
