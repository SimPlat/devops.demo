package simos.devops.demo;

import io.lettuce.core.ClientOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import simos.devops.demo.models.Hit;

import java.time.LocalDateTime;

@Configuration
public class RedisConfiguration {
	
	private final String url;
	private final int port;
	private final String password;
	
	public RedisConfiguration(@Value("${spring.redis.host}") String url,
							  @Value("${spring.redis.port}") int port,
							  @Value("${spring.redis.password}") String password) {
		this.url = url;
		this.port = port;
		this.password = password;
	}
	
	public @Bean RedisStandaloneConfiguration redisStandaloneConfiguration() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(url, port);
		redisStandaloneConfiguration.setPassword(password);
		return redisStandaloneConfiguration;
	}
	
	public @Bean ClientOptions clientOptions() {
		return ClientOptions.builder()
			.disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS)
			.autoReconnect(true)
			.build();
	}
	
	public @Bean RedisConnectionFactory redisConnectionFactory(){
		LettuceClientConfiguration clientConfiguration = LettuceClientConfiguration.builder().clientOptions(clientOptions()).build();
		return new LettuceConnectionFactory(redisStandaloneConfiguration(), clientConfiguration);
	}
	
	public @Bean RedisTemplate<String, Hit> redisHitTemplate(){
		final RedisTemplate<String, Hit> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory());
		template.setValueSerializer(new GenericToStringSerializer<>(Hit.class));
		return template;
	}

	public @Bean RedisTemplate<String, Integer> redisCounterTemplate(){
		final RedisTemplate<String, Integer> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory());
		template.setValueSerializer(new GenericToStringSerializer<>(Integer.class));
		return template;
	}
	
	public @Bean HashOperations<String , Integer, LocalDateTime> getHashOpsFromHitTemplate(){ return redisHitTemplate().opsForHash(); }
	
	public @Bean ValueOperations<String , Integer> getCounterOpsFromCounterTemplate(){ return redisCounterTemplate().opsForValue(); }
	
}
